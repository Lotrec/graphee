<?php
include "pdo.php";
include_once 'debug.php';

function create($name){
  global $pdo;
  $req = $pdo->prepare('insert into people (name) values (?);');
  $req->execute([$name]);
}

function delete(){
  global $pdo;
  $req = $pdo->prepare("delete  from people;");
  $req->execute();
}