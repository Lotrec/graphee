DROP database if exists lesgens;

create database lesgens;
use lesgens;

CREATE TABLE people (
    id INT auto_increment NOT NULL Primary key,
    name VARCHAR(200) NOT NULL
);

CREATE TABLE contact (
    people1 int not null,
    people2 int not null,
    FOREIGN KEY (people1) REFERENCES people(id) ON DELETE CASCADE,
    FOREIGN KEY (people2) REFERENCES people(id)  ON DELETE CASCADE
);

DROP USER IF EXISTS 'tousStopAntiCovid'@'127.0.0.1';
CREATE USER 'tousStopAntiCovid'@'127.0.0.1' IDENTIFIED BY 'admin123';
GRANT ALL PRIVILEGES ON lesgens.* TO 'tousStopAntiCovid'@'127.0.0.1' ;