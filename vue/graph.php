<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Network</title>

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js" integrity="sha384-w76AqPfDkMBDXo30jS1Sgez6pr3x5MlQ1ZAGC+nuZB+EYdgRZgiwxhTBTkF7CXvN" crossorigin="anonymous"></script>

    <script
      type="text/javascript"
      src="https://unpkg.com/vis-network/standalone/umd/vis-network.min.js"
    ></script>
    <style type="text/css">
      #mynetwork {
        width: 600px;
        height: 400px;
        border: 1px solid lightgray;
      }
    </style>
  </head>
  <body>

  <?php include "header.php";?>
  
    <div id="mynetwork"></div>
    <script type="text/javascript">
        // faire un tableau vide nodes
        var nodes = new vis.DataSet([]);
        // faire un tableau vide edges
        var edges = new vis.DataSet([]);
        // charger la data json au lieu de faire un copypaste dégueulasse 
        // donc go faire un fetch à partir de ici:
        fetch('../contacts.json')
            .then(response => response.json())
            .then(dataObj=> {
                // travailler ce fetch :OK
                // faire que people aille dans nodes
                for (var i = 0; i < dataObj.people.length; i++) {
                    nodes.add({ id: dataObj.people[i].id, label: dataObj.people[i].name });
                }
                // faire que contact aille dans edges
                for (var j = 0; j < dataObj.contact.length; j++) {
                    edges.add({ from: dataObj.contact[j].from, to: dataObj.contact[j].to });
                }
                // laisser ce truc en paix car il crée le network 
                var container = document.getElementById("mynetwork");
                var data = {
                    nodes: nodes,
                    edges: edges,
                };
                var options = {};
                var network = new vis.Network(container, data, options);
            });
    </script>
  </body>
</html>
