<?php
include 'pdo.php';


function createContact($people_one, $people_two){
    global $pdo;
    $req = $pdo->prepare("insert into contact (people_one, people_two) values (?, ?);");
    $req->execute([$people_one, $people_two]);
};


function readContact($id){
    global $pdo;
    $req = $pdo->prepare("select * from contact where id=?;");
    $req->execute([$id]);
    return $req->fetchAll();
};


function readAllContact(){
    global $pdo;
    $req = $pdo->prepare("select * from contact ;");
    $req->execute(); 
    return $req->fetchAll();
};


function deleteContact($id){
    global $pdo;
    $req = $pdo->prepare("delete from contact where id=?;");
    $req->execute([$id]);
};



$reqPeople = $pdo->query("SELECT * from people;");
$people = $reqPeople->fetchAll();

?>