use lesgens ,




insert into people (name) values ("Sunshine") ;
insert into people (name) values ("Peppa Pig") ;
insert into people (name) values ("Red Velvet") ;
insert into people (name) values ("Chance") ;
insert into people (name) values ("Einstein") ;
insert into people (name) values ("Starbuck") ;
insert into people (name) values ("Amiga") ;
insert into people (name) values ("Flower") ;
insert into people (name) values ("PB&J") ;
insert into people (name) values ("Tough Guy") ;
insert into people (name) values ("Foxy") ;
insert into people (name) values ("Robin") ;
insert into people (name) values ("Cumulus") ;
insert into people (name) values ("cheese") ;
insert into people (name) values ("Sourdough") ;
insert into people (name) values ("Ms. Congeniality") ;
insert into people (name) values ("Ghoulie") ;
insert into people (name) values ("Sherlock") ;
insert into people (name) values ("Bambino") ;
insert into people (name) values ("Foxy Lady") ;
insert into people (name) values ("MomBod") ;
insert into people (name) values ("Tater Tot") ;
insert into people (name) values ("Dino") ;
insert into people (name) values ("Little Bear") ;
insert into people (name) values ("Lil Girl") ;
insert into people (name) values ("Smarty") ;
insert into people (name) values ("Chickie") ;
insert into people (name) values ("Grease") ;
insert into people (name) values ("Muscles") ;
insert into people (name) values ("Weiner") ;
insert into people (name) values ("Donut") ;
insert into people (name) values ("Stud") ;
insert into people (name) values ("Chuckles") ;
insert into people (name) values ("Chef") ;
insert into people (name) values ("Gordo") ;
insert into people (name) values ("4-Wheel") ;
insert into people (name) values ("Queenie") ;
insert into people (name) values ("Friendo") ;
insert into people (name) values ("umi") ;
insert into people (name) values ("Amour") ;
insert into people (name) values ("Chico") ;
insert into people (name) values ("Babs") ;
insert into people (name) values ("Bean") ;
insert into people (name) values ("Diet Coke") ;
insert into people (name) values ("Frogger") ;
insert into people (name) values ("Green Giant") ;
insert into people (name) values ("Turkey") ;
insert into people (name) values ("Terminator");
insert into people (name) values ("Tickles") ;
insert into people (name) values ("Dropout");

INSERT INTO contact (people1,people2)
VALUES
    (48,32),
    (12,22),
    (18,10),
    (8,25),
    (26,37),
    (32,33),
    (5,40),
    (44,32),
    (1,13),
    (25,44);
INSERT INTO contact (people1,people2)
VALUES
    (25,42),
    (29,25),
    (7,47),
    (16,47),
    (49,22),
    (30,27),
    (44,41),
    (38,40),
    (21,48),
    (33,16);
INSERT INTO contact (people1,people2)
VALUES
    (28,45),
    (38,28),
    (38,49),
    (17,25),
    (48,39),
    (44,24),
    (3,28),
    (40,28),
    (27,11),
    (15,10);
INSERT INTO contact (people1,people2)
VALUES
    (34,6),
    (16,11),
    (11,3),
    (4,4),
    (21,26),
    (6,41),
    (29,7),
    (20,44),
    (27,12),
    (15,45);
INSERT INTO contact (people1,people2)
VALUES
    (43,5),
    (36,44),
    (25,40),
    (39,33),
    (36,8),
    (17,41),
    (1,28),
    (37,48),
    (47,48),
    (36,2);
INSERT INTO contact (people1,people2)
VALUES
    (45,22),
    (33,23),
    (18,19),
    (13,44),
    (27,14),
    (29,33),
    (41,27),
    (33,20),
    (8,17),
    (32,44);
INSERT INTO contact (people1,people2)
VALUES
    (47,47),
    (15,44),
    (12,28),
    (13,41),
    (49,15),
    (25,18),
    (24,40),
    (33,37),
    (35,9),
    (49,15);
INSERT INTO contact (people1,people2)
VALUES
    (41,25),
    (40,8),
    (17,16),
    (22,31),
    (42,35);