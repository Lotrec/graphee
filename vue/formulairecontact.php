<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <title>Formulaire contact</title>

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js" integrity="sha384-w76AqPfDkMBDXo30jS1Sgez6pr3x5MlQ1ZAGC+nuZB+EYdgRZgiwxhTBTkF7CXvN" crossorigin="anonymous"></script>

</head>
<body>
<?php
include_once "../MODEL/debug.php";
include_once "../MODEL/readAll.php";
?>

  <form action="../CONTROL/enregistrer_contact.php" method="post" class="row">
    <div class="container">
      <div class="liste">
        <div class="column" id="colonne_gauche">
          <h2>les gens à gauche:</h2>
          <select name="people1" id="people1">
            <option value="">Sélectionnez une personne</option>
              <?php foreach($people as $person){ ?>
                <option value="<?= $person['name'] ?>"><?= $person['name'] ?></option>
              <?php } ?>
          </select>
        </div>
        <div class="column" id="colonne_droite">
          <h2>les gens à droite:</h2>
          <select name="people2" id="people2">
          <option value="">Sélectionnez une personne</option>
              <?php foreach($people as $person){ ?>
                <option value="<?= $person['name'] ?>"><?= $person['name'] ?></option>
              <?php } ?>
          </select>
        </div>
      </div>  
      <input type="submit" value="Valider">
    </div>
  </form>
</body>
</html>