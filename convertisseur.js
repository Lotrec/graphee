const mysql = require('mysql');
const fs = require('fs');

const connection = mysql.createConnection({
  host: '127.0.0.1',
  user: 'tousStopAntiCovid',
  password: 'admin123',
  database: 'lesgens'
});

connection.connect();


connection.query('SELECT * FROM people', function (error, results, fields) {
  if (error) throw error;

  const jsonData = JSON.stringify(results);

  fs.writeFile('contacts.json', jsonData, function (error) {
    if (error) throw error;
    console.log('Données enregistrées dans contacts.json');
  });
});

// Ferme la connexion à la base de données MySQL
connection.end();
