<?php    
include "../MODEL/debug.php";
include "../MODEL/model.php";
include "../MODEL/pdo.php";

if ($_SERVER['REQUEST_METHOD'] === 'POST') {

    $people1 = $_POST['people1'];
    $people2 = $_POST['people2'];
    
    // Utilisation des noms de paramètres différents pour les deux variables
    $stmt = $pdo->prepare('INSERT INTO contact (people_one, people_two) VALUES ((SELECT id FROM people WHERE name = :people1), (SELECT id FROM people WHERE name = :people2))');
    $stmt->bindParam(':people1', $people1);
    $stmt->bindParam(':people2', $people2);
    $stmt->execute();

}