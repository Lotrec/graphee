import { createConnection } from 'mysql';
import { writeFile } from 'fs';

const connection = createConnection({
  host: '127.0.0.1',
  user: 'tousStopAntiCovid',
  password: 'admin123',
  database: 'lesgens'
});

connection.connect();

connection.query('SELECT people.id as person_id, people.name as person_name, contact.id as contact_id, contact.people_one, contact.people_two FROM people LEFT JOIN contact ON people.id = contact.people_one OR people.id = contact.people_two', function (error, results, fields) {
  if (error) throw error;

  const data = {people: [], contact: []};

  // Création de la liste des personnes
  const peopleMap = new Map();
  for (const result of results) {
    const personId = result.person_id;
    if (!peopleMap.has(personId)) {
      peopleMap.set(personId, { id: personId, name: result.person_name });
    }
  }
  data.people = [...peopleMap.values()];

  // Création de la liste des contacts
  for (const result of results) {
    const person1Id = result.people_one;
    const person2Id = result.people_two;
    if (person1Id && person2Id) {
      data.contact.push({ from: person1Id, to: person2Id });
    }
  }

  const jsonData = JSON.stringify(data);

  writeFile('contacts.json', jsonData, function (error) {
    if (error) throw error;
    console.log('Données enregistrées dans contacts.json');
  });
});

connection.end();
